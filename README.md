AWS :cloud:, Pulumi :wrench:, Python :snake: and CI/CD :fox:

**Intro**
This repository create a Flaks api using Docker, Fargate, ECR and ECS. The `./web` folder has all of the neccessary files for this purpose. This api can be tested using `postman`. By posting two numbers this api can do /Add, /Multiply, /Subtract and /Divide. The mani page explain the infrastructure. Copy and paste the URL created via pulumi and test the api as in **postman**

                                                                        **postman**


![](postman.PNG)

**The structure**:
- Pushing all data into Gitlab and creating a pipeline using `.gitlab-ci.yml`.
- Creating a Docker image using Dockerfile in `./web`. The image is created using Dockerbuild function in pulumi.
- registring the created image using Amazone ECR.
- Creating a load balancer on port 80 to watch the incoming traffic.
- Pushing the created image into Amazone Fargate.
- Using ECS as Docker orchestrator on top of created Fargate.
- lunching the infrastructure and api is ready to useas in **postman**


                                                                        **Structure**

![](Structure.PNG)

**Architecture**:
- The api is created via Flask in Pycharm :snake:
- The filrs are pusshed into Gitlab and by using CI/CD it is feed into the pipeline.
- For post method only it takes 2 numbwers as input as show the `./Add`, `./Multiply`, `./Subtract` and `./Divide`
- initially it installs Pulumi, Python and awscli.
- The the pipeline preview the whole VPC infrastructure.
- if the preview is successful then at the next stage of the pipeline it runs and deploy the api on AWS.
                                        **Architecture**

![](architecture.png)


**Notes:** :speech_balloon:

I have used AWS account for the purpuse of this project.

Feel free to run `pulumi destroy --yes` command to delete all of deployed infrastructure.

the whole IaC steps are automatized. Only AWS credentials need to be set on gitLab in order for the pipeline to run successfully
